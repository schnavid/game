use crate::{
    graphics::{
        backend::Backend,
        Drawable
    }
};
use crate::graphics::DrawOptions;
use crate::game_state::GameContext;

pub trait SpriteBatch<B: Backend>: Drawable<B> {
    fn new(texture: u32, ctx: &mut GameContext<B>) -> Self;
    fn add(&mut self, position: [u32; 2], size: [u32; 2], draw_ops: DrawOptions);
}
