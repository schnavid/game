use std::fmt::Debug;

pub trait Texture: Debug {
    fn name(&self) -> String;
    fn dimensions(&self) -> (u32, u32);
}