use std::fmt::Debug;
use crate::{
    graphics::{
        color::Color,
        backend::Backend,
    },
    game_state::{GameResult, GameContext},
};
use crate::utils::Vector2;

// pub mod vulkan;
pub mod opengl;

pub mod color;
pub mod backend;
pub mod sprite_batch;
pub mod texture;

#[derive(Debug)]
pub enum DisplayMode {
    Windowed(u16, u16),
    Fullscreen,
}

pub trait Drawable<B: Backend> {
    fn vertices(&self, ctx: &mut GameContext<B>) -> Vec<B::Vertex>;
    fn indices(&self, ctx: &mut GameContext<B>) -> Vec<u16>;
    fn primitive_type(&self, ctx: &mut GameContext<B>) -> B::PrimitiveType;
    fn texture(&self) -> u32;
}

pub struct DrawOptions {
    position: Vector2,
    color: Color,
}

impl Default for DrawOptions {
    fn default() -> Self {
        DrawOptions {
            position: [0.0, 0.0].into(),
            color: color::WHITE,
        }
    }
}

impl DrawOptions {
    pub fn position<V2: Into<Vector2>>(mut self, position: V2) -> Self {
        self.position = position.into();
        self
    }

    pub fn color(mut self, color: Color) -> Self {
        self.color = color;
        self
    }
}

pub trait GraphicsError: Debug {}

pub trait Camera<G: Backend> {
    fn matrix(&self) -> G::Matrix;
    fn set_position<V2: Into<Vector2>>(&mut self, position: V2);
    fn set_aspect_ratio(&mut self, ratio: f32);
    fn set_zoom(&mut self, zoom: f32);
}

pub fn dimensions<G: Backend>(ctx: &mut GameContext<G>) -> (u32, u32) {
    ctx.0.dimensions()
}

pub fn update_and_use_camera<G: Backend, C: Camera<G>>(ctx: &mut GameContext<G>, camera: &mut C) {
    let aspect_ratio = {
        let (w, h) = dimensions(ctx);

        w as f32 / h as f32
    };
    camera.set_aspect_ratio(aspect_ratio);
    use_camera(ctx, camera);
}

pub fn use_camera<G: Backend, C: Camera<G>>(ctx: &mut GameContext<G>, camera: &C) {
    ctx.0.use_matrix(camera.matrix());
}

pub fn clear_color<G: Backend>(ctx: &mut GameContext<G>, red: u8, green: u8, blue: u8) -> GameResult<(), G> {
    ctx.0.clear_color(red, green, blue)
}

pub fn draw<G: Backend, D: Drawable<G>>(ctx: &mut GameContext<G>, drawable: &D, draw_ops: DrawOptions) -> GameResult<(), G> {
    let vertices = drawable.vertices(ctx);
    let indices = drawable.indices(ctx);
    let texture = drawable.texture();
    let primitive_type = drawable.primitive_type(ctx);
    ctx.0.draw(vertices, indices, texture, primitive_type, draw_ops)
}