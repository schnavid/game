#[derive(Copy, Clone)]
pub struct Color(f32, f32, f32, f32);

impl Color {
    pub fn red_lin(&self) -> f32 {
        self.0
    }

    pub fn green_lin(&self) -> f32 {
        self.1
    }

    pub fn blue_lin(&self) -> f32 {
        self.2
    }

    pub fn alpha_lin(&self) -> f32 {
        self.3
    }
}

impl From<(u8, u8, u8, u8)> for Color {
    fn from((r, g, b, a): (u8, u8, u8, u8)) -> Self {
        Color(r as f32 / 256.0, g as f32 / 256.0, b as f32 / 256.0, a as f32 / 256.0)
    }
}

impl From<(f32, f32, f32, f32)> for Color {
    fn from((r, g, b, a): (f32, f32, f32, f32)) -> Self {
        Color(r, g, b, a)
    }
}

impl From<[f32; 4]> for Color {
    fn from(values: [f32; 4]) -> Self {
        Color(values[0], values[1], values[2], values[3])
    }
}

pub const WHITE: Color = Color(1.0, 1.0, 1.0, 1.0);
pub const BLACK: Color = Color(0.0, 0.0, 0.0, 1.0);
