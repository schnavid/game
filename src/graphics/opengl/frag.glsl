#version 140

in vec4 v_color;
in vec2 v_tex_coords;

uniform sampler2D sampler;

out vec4 color;

void main() {
    color = texture(sampler, v_tex_coords);
}
