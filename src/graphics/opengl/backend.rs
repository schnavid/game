use crate::{game_state::{GameContext, GameError, GameResult, GameState}, graphics::{
    backend::{Backend, BackendBuilder},
    opengl::{
        camera::identity,
        error::OpenGLError,
        vertex::Vertex,
        texture::Texture as GLTexture,
    },
    DisplayMode,
    DrawOptions,
    texture::Texture,
}, graphics};
use glium::{backend::glutin::glutin::{Event, EventsLoop, WindowBuilder, WindowEvent}, glutin::{dpi::LogicalSize, ContextBuilder}, index::PrimitiveType, texture::{RawImage2d, SrgbTexture2d}, uniforms::{MagnifySamplerFilter, MinifySamplerFilter, Sampler}, Display, DrawParameters, Frame, IndexBuffer, Program, Surface, VertexBuffer, Blend};
use image::RgbaImage;
use std::fmt::{Debug, Error, Formatter};
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::rc::Rc;
use glium::backend::glutin::glutin::{VirtualKeyCode, ElementState};

#[derive(Debug)]
pub struct OpenGLBackendBuilder {
    title: String,
    display_mode: DisplayMode,
}

impl Default for OpenGLBackendBuilder {
    fn default() -> Self {
        OpenGLBackendBuilder {
            title: "game".to_string(),
            display_mode: DisplayMode::Fullscreen,
        }
    }
}

impl BackendBuilder<OpenGLBackend> for OpenGLBackendBuilder {
    fn display_mode(self, display_mode: DisplayMode) -> Self {
        OpenGLBackendBuilder {
            display_mode,
            ..self
        }
    }

    fn title<S: Into<String>>(self, title: S) -> Self {
        OpenGLBackendBuilder {
            title: title.into(),
            ..self
        }
    }

    fn build(self) -> Result<(OpenGLBackend, EventsLoop), OpenGLError> {
        let events_loop = EventsLoop::new();

        let window_builder = WindowBuilder::new();
        let window_builder = match self.display_mode {
            DisplayMode::Fullscreen => unimplemented!(),
            DisplayMode::Windowed(w, h) => {
                window_builder.with_dimensions(LogicalSize::from((w as u32, h as u32)))
            }
        };
        let window_builder = window_builder.with_title(self.title);

        let gl_context_builder = ContextBuilder::new().with_depth_buffer(24);

        let display = Display::new(window_builder, gl_context_builder, &events_loop)?;

        let vertex_shader_src = include_str!("vert.glsl");
        let fragment_shader_src = include_str!("frag.glsl");
        let program =
            glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None)
                .unwrap();

        Ok((
            OpenGLBackend {
                display,
                target: None,
                camera_mat: None,
                program,
                textures: Default::default(),
                textures_counter: 0,
                keys: Vec::new(),
                pressed_keys: Vec::new()
            },
            events_loop,
        ))
    }
}

pub struct OpenGLBackend {
    display: Display,
    target: Option<Frame>,
    camera_mat: Option<[[f32; 4]; 4]>,
    program: Program,
    textures: HashMap<u32, Rc<GLTexture>>,
    textures_counter: u32,

    keys: Vec<VirtualKeyCode>,
    pressed_keys: Vec<VirtualKeyCode>,
}

impl OpenGLBackend {
    pub fn make_texture(&mut self, texture: RgbaImage) -> SrgbTexture2d {
        let dimensions = texture.dimensions();
        let image = RawImage2d::from_raw_rgba_reversed(&texture.into_raw(), dimensions);
        SrgbTexture2d::new(&self.display, image).unwrap()
    }
}

impl Debug for OpenGLBackend {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "OpenGLBackend {{ .. }}")
    }
}

impl graphics::backend::PrimitiveType for PrimitiveType {
    fn triangles() -> Self {
        PrimitiveType::TrianglesList
    }
}

impl Backend for OpenGLBackend {
    type Builder = OpenGLBackendBuilder;
    type EventLoop = EventsLoop;
    type Error = OpenGLError;
    type Vertex = Vertex;
    type PrimitiveType = PrimitiveType;
    type Texture = GLTexture;
    type Matrix = [[f32; 4]; 4];

    fn new() -> Self::Builder {
        OpenGLBackendBuilder::default()
    }

    fn run<S: GameState<Self>>(
        mut ctx: GameContext<Self>,
        mut events_loop: Self::EventLoop,
    ) -> GameResult<(), Self> {
        let mut state = S::init(&mut ctx)?;

        let mut quit = false;
        while !quit {
            ctx.0.pressed_keys.clear();
            events_loop.poll_events(|event| match event {
                Event::WindowEvent { event, .. } => match &event {
                    WindowEvent::CloseRequested => quit = true,
                    WindowEvent::KeyboardInput { input, .. } => {
                        let (keycode, _modifier, state) = (input.virtual_keycode, input.modifiers, input.state);

                        if let Some(keycode) = keycode {
                            match state {
                                ElementState::Pressed => {
                                    if !ctx.0.keys.contains(&keycode) {
                                        ctx.0.keys.push(keycode);
                                        ctx.0.pressed_keys.push(keycode);
                                    }
                                }
                                ElementState::Released => {
                                    if let Some((i, _)) = ctx.0.keys.iter().enumerate().find(|(_, item)| **item == keycode) {
                                        ctx.0.keys.remove(i);
                                    }
                                }
                            }
                        }
                    }
                    _ => {}
                },
                Event::Suspended(_pause) => {}
                _ => {}
            });

            state.update(&mut ctx)?;

            ctx.0.target = Some(ctx.0.display.draw());

            state.draw(&mut ctx)?;

            ctx.0.target.unwrap().finish().unwrap();
            ctx.0.target = None;
        }

        Ok(())
    }

    fn clear_color(&mut self, red: u8, green: u8, blue: u8) -> GameResult<(), OpenGLBackend> {
        if let Some(target) = &mut self.target {
            target.clear_color(
                red as f32 / 255.0,
                green as f32 / 255.0,
                blue as f32 / 255.0,
                1.0,
            );
            target.clear_depth(1.0);
            Ok(())
        } else {
            Err(GameError::GraphicsError(OpenGLError::TargetInactive))
        }
    }

    fn draw(&mut self, mut vertices: Vec<Self::Vertex>, indices: Vec<u16>, texture: u32, primitive_type: Self::PrimitiveType, draw_ops: DrawOptions) -> Result<(), GameError<Self>> {
        let vertex_buffer = VertexBuffer::new(
            &self.display,
            vertices.iter_mut().map(|x| {
                x.position[0] += draw_ops.position.x;
                x.position[1] += draw_ops.position.y;
                *x
            }).collect::<Vec<Vertex>>().as_slice())
            .map_err(|e| GameError::GraphicsError(e.into()))?;

        let index_buffer = IndexBuffer::new(
            &self.display,
            primitive_type,
            indices.as_ref(),
        )
            .map_err(|e| GameError::GraphicsError(e.into()))?;

        let texture = self.get_texture(texture);

        if let Some(target) = &mut self.target {
            target
                .draw(
                    &vertex_buffer,
                    &index_buffer,
                    &self.program,
                    &uniform!(
                        sampler: Sampler::new(&texture.0)
                            .magnify_filter(MagnifySamplerFilter::Nearest)
                            .minify_filter(MinifySamplerFilter::Nearest),
                        perspective: self.camera_mat.unwrap_or_else(identity)
                    ),
                    &DrawParameters {
                        blend: Blend::alpha_blending(),
                        ..Default::default()
                    },
                )
                .unwrap();
            Ok(())
        } else {
            Err(GameError::GraphicsError(OpenGLError::TargetInactive))
        }
    }

    fn use_matrix(&mut self, matrix: Self::Matrix) {
        self.camera_mat = Some(matrix);
    }

    fn dimensions(&self) -> (u32, u32) {
        if let Some(t) = &self.target {
            t.get_dimensions()
        } else {
            (0, 0)
        }
    }

    fn get_texture(&self, id: u32) -> Rc<Self::Texture> {
        self.textures.get(&id).unwrap().clone()
    }

    fn load_texture<S: Into<String>>(&mut self, name: S) -> Result<u32, GameError<OpenGLBackend>> {
        let name = name.into();
        if let Some((id, _)) = self.textures.iter().find(|(_, t)| t.name() == name.clone()) {
            Ok(*id)
        } else {
            let file = File::open("resources/textures.yml").map_err(|_| GameError::ResourceFileNotFound("textures".to_string()))?;
            let map: HashMap<String, String> = serde_yaml::from_reader(file).map_err(|_| GameError::InvalidTexturesFile)?;
            let texture_path = map.get(&name).ok_or_else(|| GameError::TextureNotFound(name.clone()))?;

            let image = image::load(
                BufReader::new(
                    File::open("resources/".to_string() + texture_path)
                        .map_err(|_| GameError::TextureNotFound(name.clone()))?
                ),
                image::PNG,
            ).unwrap().to_rgba();

            let texture = self.make_texture(image);

            self.textures.insert(self.textures_counter, Rc::new(GLTexture(texture, name.clone())));

            self.textures_counter += 1;

            Ok(self.textures_counter - 1)
        }
    }
}
