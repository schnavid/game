use glium::texture::SrgbTexture2d;
use crate::graphics;

#[derive(Debug)]
pub struct Texture(pub(crate) SrgbTexture2d, pub(crate) String);

impl graphics::texture::Texture for Texture {
    fn name(&self) -> String {
        self.1.clone()
    }

    fn dimensions(&self) -> (u32, u32) {
        self.0.dimensions()
    }
}