use crate::graphics::color::Color;
use crate::graphics::backend;

#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: [f32; 3],
    pub color: [f32; 4],
    pub tex_coords: [f32; 2],
}

impl backend::Vertex for Vertex {
    fn new(position: [f32; 3], color: Color, tex_coords: [f32; 2]) -> Vertex {
        Vertex {
            position,
            color: [color.red_lin(), color.green_lin(), color.blue_lin(), color.alpha_lin()],
            tex_coords,
        }
    }

    fn position(&self) -> [f32; 3] {
        self.position
    }

    fn color(&self) -> Color {
        Color::from(self.color)
    }

    fn tex_coords(&self) -> [f32; 2] {
        self.tex_coords
    }
}

impl From<([f32; 3], Color, [f32; 2])> for Vertex {
    fn from((position, color, tex_coords): ([f32; 3], Color, [f32; 2])) -> Self {
        Vertex {
            position,
            color: [color.red_lin(), color.green_lin(), color.blue_lin(), color.alpha_lin()],
            tex_coords,
        }
    }
}

implement_vertex!(Vertex, position, color, tex_coords);
