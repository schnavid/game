use crate::{
    game_state::GameContext,
    graphics::{
        opengl::vertex::Vertex as GLVertex,
        backend::Backend,
        opengl::backend::OpenGLBackend,
        sprite_batch,
        DrawOptions,
        Drawable,
        backend::Vertex
    }
};
use glium::index::PrimitiveType;

pub struct Sprite {
    pub position: [u32; 2],
    pub size: [u32; 2],
    pub draw_opts: DrawOptions,
}

impl Sprite {
    pub fn new(position: [u32; 2], size: [u32; 2], draw_opts: DrawOptions) -> Sprite {
        Sprite {
            position,
            size,
            draw_opts,
        }
    }
}

pub struct SpriteBatch {
    texture: u32,
    sprites: Vec<Sprite>,
}

impl SpriteBatch {
    pub fn reset(&mut self) {
        self.sprites.clear();
    }
}

impl Drawable<OpenGLBackend> for SpriteBatch {
    fn vertices(&self, ctx: &mut GameContext<OpenGLBackend>) -> Vec<<OpenGLBackend as Backend>::Vertex> {
        let texture = ctx.0.get_texture(self.texture);

        let (w, h) = texture.0.dimensions();

        let mut result = Vec::with_capacity(self.sprites.len() * 4);
        for sprite in &self.sprites {
            result.push(GLVertex::new(
                [
                    sprite.draw_opts.position.x,
                    sprite.draw_opts.position.y,
                    1.0,
                ],
                sprite.draw_opts.color,
                [
                    sprite.position[0] as f32 / w as f32,
                    (h - sprite.position[1]) as f32 / h as f32,
                ],
            ));
            result.push(GLVertex::new(
                [
                    sprite.draw_opts.position.x + sprite.size[0] as f32,
                    sprite.draw_opts.position.y,
                    1.0,
                ],
                sprite.draw_opts.color,
                [
                    (sprite.position[0] + sprite.size[0]) as f32 / w as f32,
                    (h - sprite.position[1]) as f32 / h as f32,
                ],
            ));
            result.push(GLVertex::new(
                [
                    sprite.draw_opts.position.x + sprite.size[0] as f32,
                    (sprite.draw_opts.position.y + sprite.size[1] as f32),
                    1.0,
                ],
                sprite.draw_opts.color,
                [
                    (sprite.position[0] + sprite.size[0]) as f32 / w as f32,
                    (h - (sprite.position[1] + sprite.size[1])) as f32 / h as f32,
                ],
            ));
            result.push(GLVertex::new(
                [
                    sprite.draw_opts.position.x,
                    (sprite.draw_opts.position.y + sprite.size[1] as f32),
                    1.0,
                ],
                sprite.draw_opts.color,
                [
                    sprite.position[0] as f32 / w as f32,
                    (h - (sprite.position[1] + sprite.size[1])) as f32 / h as f32,
                ],
            ));
        }
        result
    }

    fn indices(&self, _: &mut GameContext<OpenGLBackend>) -> Vec<u16> {
        let mut result = Vec::with_capacity(self.sprites.len() * 6);
        for (i, _) in self.sprites.iter().enumerate().map(|(i, s)| (i * 4, s)) {
            let i = i as u16;
            result.append(&mut vec![i, i + 1, i + 2, i, i + 2, i + 3]);
        }
        result
    }

    fn primitive_type(&self, _: &mut GameContext<OpenGLBackend>) -> <OpenGLBackend as Backend>::PrimitiveType {
        PrimitiveType::TrianglesList
    }

    fn texture(&self) -> u32 {
        self.texture
    }
}

impl sprite_batch::SpriteBatch<OpenGLBackend> for SpriteBatch {
    fn new(texture: u32, _: &mut GameContext<OpenGLBackend>) -> SpriteBatch {
        SpriteBatch {
            texture,
            sprites: Vec::new(),
        }
    }

    fn add(&mut self, position: [u32; 2], size: [u32; 2], draw_ops: DrawOptions) {
        self.sprites.push(Sprite::new(position, size, draw_ops))
    }
}
