use crate::graphics;
use crate::graphics::opengl::backend::OpenGLBackend;
use crate::utils::Vector2;

pub struct Camera {
    matrix: [[f32; 4]; 4],
    position: Vector2,
    aspect_ratio: f32,
    zoom: f32,
}

impl Camera {
    pub fn new() -> Camera {
        let mut c = Camera::default();
        c.recalculate_matrix();
        c
    }

    fn recalculate_matrix(&mut self) {
        let h169 = 360.;
        let w169 = 640.;

        let (left, right, top, bottom) = {
            if self.aspect_ratio < 1.0 {
                let bottom = self.position.y + (h169 / 2.0) / self.zoom;
                let top = self.position.y - (h169 / 2.0) / self.zoom;

                let left = self.position.x - (h169 * self.aspect_ratio / 2.0) / self.zoom;
                let right = self.position.x + (h169 * self.aspect_ratio / 2.0) / self.zoom;

                (left, right, top, bottom)
            } else {
                let bottom = self.position.y + (w169 / self.aspect_ratio / 2.0) / self.zoom;
                let top = self.position.y - (w169 / self.aspect_ratio / 2.0) / self.zoom;

                let left = self.position.x - (w169 / 2.0) / self.zoom;
                let right = self.position.x + (w169 / 2.0) / self.zoom;

                (left, right, top, bottom)
            }
        };

        let near = 0.1;
        let far = 100.0;

        self.matrix = [
            [2.0 / (right - left), 0.0, 0.0, 0.0],
            [0.0, 2.0 / (top - bottom), 0.0, 0.0],
            [0.0, 0.0, 2.0 / (far - near), 0.0],
            [
                -(right + left) / (right - left),
                -(top + bottom) / (top - bottom),
                -(far + near) / (far - near),
                1.0f32,
            ],
        ];
    }
}

impl Default for Camera {
    fn default() -> Self {
        Camera {
            position: (0.0, 0.0).into(),
            zoom: 1.0,
            aspect_ratio: 16.0 / 9.0,
            matrix: identity(),
        }
    }
}

impl graphics::Camera<OpenGLBackend> for Camera {
    fn matrix(&self) -> [[f32; 4]; 4] {
        self.matrix
    }

    fn set_position<V2: Into<Vector2>>(&mut self, position: V2) {
        let position = position.into();
        if self.position != position {
            self.position = position;
            self.recalculate_matrix()
        }
    }

    fn set_aspect_ratio(&mut self, ratio: f32) {
        if (self.aspect_ratio - ratio).abs() > std::f32::EPSILON {
            self.aspect_ratio = ratio;
            self.recalculate_matrix()
        }
    }

    fn set_zoom(&mut self, zoom: f32) {
        if (self.zoom - zoom).abs() > std::f32::EPSILON {
            self.zoom = zoom;
            self.recalculate_matrix()
        }
    }
}

pub fn identity() -> [[f32; 4]; 4] {
    [
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ]
}
