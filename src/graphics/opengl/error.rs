use glium::{backend::glutin::DisplayCreationError, glutin::CreationError, vertex, index};

use crate::{
    graphics::{
        GraphicsError
    },
    game_state::GameError,
};
use crate::graphics::opengl::backend::OpenGLBackend;

#[derive(Debug)]
pub enum OpenGLError {
    DisplayCreationError(DisplayCreationError),
    CreationError(CreationError),
    VertexBufferCreationError(vertex::BufferCreationError),
    IndexBufferCreationError(index::BufferCreationError),
    TargetInactive,
}

impl From<DisplayCreationError> for OpenGLError {
    fn from(e: DisplayCreationError) -> Self {
        OpenGLError::DisplayCreationError(e)
    }
}

impl From<CreationError> for OpenGLError {
    fn from(e: CreationError) -> Self {
        OpenGLError::CreationError(e)
    }
}

impl From<index::BufferCreationError> for OpenGLError {
    fn from(e: index::BufferCreationError) -> Self {
        OpenGLError::IndexBufferCreationError(e)
    }
}

impl From<vertex::BufferCreationError> for OpenGLError {
    fn from(e: vertex::BufferCreationError) -> Self {
        OpenGLError::VertexBufferCreationError(e)
    }
}

impl From<OpenGLError> for GameError<OpenGLBackend> {
    fn from(e: OpenGLError) -> Self {
        GameError::GraphicsError(e)
    }
}

impl GraphicsError for OpenGLError {}

