use crate::{
    game_state::{GameContext, GameResult, GameState},
    graphics::{
        DrawOptions,
        DisplayMode,
        GraphicsError,
        texture::Texture,
    },
};
use std::fmt::Debug;
use std::rc::Rc;
use crate::graphics::color::Color;

pub trait BackendBuilder<B: Backend>: Sized + Debug {
    fn display_mode(self, display_mode: DisplayMode) -> Self;
    fn title<S: Into<String>>(self, title: S) -> Self;
    fn build(self) -> Result<(B, B::EventLoop), B::Error>;
}

pub trait Vertex: Sized {
    fn new(position: [f32; 3], color: Color, tex_coords: [f32; 2]) -> Self;
    fn position(&self) -> [f32; 3];
    fn color(&self) -> Color;
    fn tex_coords(&self) -> [f32; 2];
}

pub trait PrimitiveType: Sized {
    fn triangles() -> Self;
}

pub trait Backend: Sized + Debug {
    type Builder: BackendBuilder<Self>;
    type EventLoop;
    type Error: GraphicsError;
    type Vertex: Vertex;
    type PrimitiveType: PrimitiveType;
    type Texture: Texture;
    type Matrix;

    fn new() -> Self::Builder;
    fn run<S: GameState<Self>>(
        ctx: GameContext<Self>,
        event_loop: Self::EventLoop,
    ) -> GameResult<(), Self>;

    fn clear_color(&mut self, red: u8, green: u8, blue: u8) -> GameResult<(), Self>;
    fn draw(&mut self, vertices: Vec<Self::Vertex>, indices: Vec<u16>, texture: u32, primitive_type: Self::PrimitiveType, draw_ops: DrawOptions) -> GameResult<(), Self>;
    fn use_matrix(&mut self, matrix: Self::Matrix);
    fn dimensions(&self) -> (u32, u32);
    fn get_texture(&self, id: u32) -> Rc<Self::Texture>;
    fn load_texture<S: Into<String>>(&mut self, name: S) -> GameResult<u32, Self>;
}
