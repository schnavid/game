use crate::graphics::backend::Backend;
use crate::world::layers::tile_layer::TileLayer;
use serde::{Serialize};
use std::marker::PhantomData;
use crate::game_state::{GameContext, GameResult};
use crate::graphics::{draw, DrawOptions};

pub mod layers;

#[derive(Debug, Serialize)]
pub struct World<G: Backend> {
    layers: Vec<Layer<G>>,
}

impl<G: Backend> World<G> {
    pub fn new(layers: Vec<Layer<G>>) -> World<G> {
        World {
            layers
        }
    }

    pub fn draw(&self, ctx: &mut GameContext<G>) -> GameResult<(), G> {
        for layer in &self.layers {
            match layer {
                Layer::Tiles(t) => draw(ctx, t, DrawOptions::default())?,
                _ => {}
            }
        }

        Ok(())
    }
}

#[derive(Debug, Serialize)]
pub enum Layer<G: Backend> {
    Tiles(TileLayer),

    Phantom(PhantomData<G>)
}
