use array2d::Array2D;
use std::collections::HashMap;
use serde::ser::{Serialize, Serializer, SerializeStruct, SerializeSeq};
use crate::{
    graphics::{
        texture::Texture,
        backend::{
            Vertex as _,
            Backend,
            PrimitiveType,
        },
        Drawable,
    },
    game_state::GameContext,
};

#[derive(Debug)]
struct Tiles(Array2D<u8>);

#[derive(Debug)]
pub struct TileLayer {
    texture: u32,
    tiles: Tiles,
    map: HashMap<u8, (u32, u32)>,
    size: u32,
    depth: f32,
}

impl Serialize for Tiles {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> where
        S: Serializer {
        let mut state = serializer.serialize_seq(Some(self.0.num_rows()))?;
        for row in self.0.rows_iter() {
            state.serialize_element(&row.copied().collect::<Vec<u8>>())?;
        }
        state.end()
    }
}

impl Serialize for TileLayer {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
        where S: Serializer {
        let mut state = serializer.serialize_struct("TileLayer", 4)?;
        state.serialize_field("size", &self.size)?;
        state.serialize_field("texture", &self.texture)?;
        state.serialize_field("map", &self.map)?;
        state.serialize_field("tiles", &self.tiles)?;
        state.end()
    }
}

const AUTOTILE_TILES: [(u8, (u32, u32)); 48] = [
    (0, (7, 4)),
    (1, (7, 5)), // NO TILE
    (2, (3, 2)),
    (8, (2, 3)),
    (10, (7, 1)),
    (11, (2, 2)),
    (16, (0, 3)),
    (18, (6, 1)),
    (22, (0, 2)),
    (24, (1, 3)),
    (26, (1, 5)),
    (27, (0, 5)),
    (30, (2, 5)),
    (31, (1, 2)),
    (64, (3, 0)),
    (66, (3, 1)),
    (72, (7, 0)),
    (74, (5, 1)),
    (75, (5, 0)),
    (80, (6, 0)),
    (82, (4, 1)),
    (86, (4, 0)),
    (88, (1, 4)),
    (90, (4, 4)),
    (91, (6, 2)),
    (94, (6, 3)),
    (95, (4, 3)),
    (104, (2, 0)),
    (106, (5, 2)),
    (107, (2, 1)),
    (120, (0, 4)),
    (122, (6, 5)),
    (123, (3, 4)),
    (126, (7, 3)),
    (127, (3, 3)),
    (208, (0, 0)),
    (210, (4, 2)),
    (214, (0, 1)),
    (216, (2, 4)),
    (218, (6, 5)),
    (219, (7, 2)),
    (222, (5, 4)),
    (223, (5, 3)),
    (248, (1, 0)),
    (250, (4, 5)),
    (251, (3, 5)),
    (254, (5, 5)),
    (255, (1, 1))
];

pub fn autotile_map(position: (u32, u32), size: u32) -> HashMap<u8, (u32, u32)> {
    AUTOTILE_TILES.iter().map(|(id, (x, y))| (*id, (position.0 + *x * size, position.1 + *y * size))).collect()
}

impl TileLayer {
    pub fn new<A: Into<Array2D<u8>>>(texture: u32, tiles: A, map: HashMap<u8, (u32, u32)>, size: u32, depth: f32) -> TileLayer {
        TileLayer {
            texture,
            tiles: Tiles(tiles.into()),
            map,
            size,
            depth,
        }
    }

    pub fn autotile<A: Into<Array2D<bool>>>(texture: u32, bool_tiles: A, position: (u32, u32), size: u32, depth: f32) -> TileLayer {
        let bool_tiles = bool_tiles.into();
        let mut tiles = Vec::with_capacity(bool_tiles.num_elements());

        for (y, row) in bool_tiles.rows_iter().enumerate() {
            for (x, element) in row.enumerate() {
                if *element {
                    let top = if y == 0 { false } else { *bool_tiles.get(y - 1, x).unwrap() };
                    let left = if x == 0 { false } else { *bool_tiles.get(y, x - 1).unwrap() };
                    let right = if x == bool_tiles.num_columns() - 1 { false } else { *bool_tiles.get(y, x + 1).unwrap() };
                    let bottom = if y == bool_tiles.num_rows() - 1 { false } else { *bool_tiles.get(y + 1, x).unwrap() };

                    let top_left = if !left || !top || y == 0 || x == 0 { false } else { *bool_tiles.get(y - 1, x - 1).unwrap() };
                    let top_right = if !right || !top || y == 0 || x == bool_tiles.num_columns() - 1 { false } else { *bool_tiles.get(y - 1, x + 1).unwrap() };
                    let bottom_left = if !left || !bottom || y == bool_tiles.num_rows() - 1 || x == 0 { false } else { *bool_tiles.get(y + 1, x - 1).unwrap() };
                    let bottom_right = if !right || !bottom || y == bool_tiles.num_rows() - 1 || x == bool_tiles.num_columns() - 1 { false } else { *bool_tiles.get(y + 1, x + 1).unwrap() };

                    let num = top_left as u8 + 2 * top as u8 + 4 * top_right as u8 + 8 * left as u8 + 16 * right as u8 + 32 * bottom_left as u8 + 64 * bottom as u8 + 128 * bottom_right as u8;

                    tiles.push(num);
                } else {
                    tiles.push(1);
                }
            }
        }

        TileLayer::new(texture, Array2D::from_row_major(tiles.as_slice(), bool_tiles.num_rows(), bool_tiles.num_columns()), autotile_map(position, size), size, depth)
    }
}

impl<B: Backend> Drawable<B> for TileLayer {
    fn vertices(&self, ctx: &mut GameContext<B>) -> Vec<B::Vertex> {
        let mut vertices = Vec::with_capacity(self.tiles.0.num_elements() * 4);
        let texture = ctx.0.get_texture(self.texture);
        let (w, h) = texture.dimensions();

        for (y, row) in self.tiles.0.rows_iter().enumerate() {
            for (x, tile) in row.enumerate() {
                let position = self.map[tile];

                vertices.push(B::Vertex::new(
                    [
                        x as f32 * self.size as f32,
                        (y as f32) * self.size as f32,
                        self.depth,
                    ],
                    (1.0, 1.0, 1.0, 1.0).into(),
                    [
                        position.0 as f32 / w as f32,
                        (h - position.1) as f32 / h as f32,
                    ],
                ));
                vertices.push(B::Vertex::new(
                    [
                        (x + 1) as f32 * self.size as f32,
                        (y as f32) * self.size as f32,
                        self.depth
                    ],
                    (1.0, 1.0, 1.0, 1.0).into(),
                    [
                        (position.0 + self.size) as f32 / w as f32,
                        (h - position.1) as f32 / h as f32,
                    ],
                ));
                vertices.push(B::Vertex::new(
                    [
                        (x + 1) as f32 * self.size as f32,
                        (y as f32 + 1.0) * self.size as f32,
                        self.depth
                    ],
                    (1.0, 1.0, 1.0, 1.0).into(),
                    [
                        (position.0 + self.size) as f32 / w as f32,
                        (h - (position.1 + self.size)) as f32 / h as f32,
                    ],
                ));
                vertices.push(B::Vertex::new(
                    [
                        x as f32 * self.size as f32,
                        (y as f32 + 1.0) * self.size as f32,
                        self.depth
                    ],
                    (1.0, 1.0, 1.0, 1.0).into(),
                    [
                        position.0 as f32 / w as f32,
                        (h - (position.1 + self.size)) as f32 / h as f32,
                    ],
                ));
            }
        }
        vertices
    }

    fn indices(&self, _ctx: &mut GameContext<B>) -> Vec<u16> {
        let mut result = Vec::with_capacity(self.tiles.0.num_elements() * 6);
        for (i, _) in self.tiles.0.elements_row_major_iter().enumerate().map(|(i, s)| (i * 4, s)) {
            let i = i as u16;
            result.append(&mut vec![i, i + 1, i + 2, i, i + 2, i + 3]);
        }
        result
    }

    fn primitive_type(&self, _ctx: &mut GameContext<B>) -> B::PrimitiveType {
        B::PrimitiveType::triangles()
    }

    fn texture(&self) -> u32 {
        self.texture
    }
}