use crate::graphics::backend::{Backend, BackendBuilder};
use legion::world::Universe;

pub struct GameContext<G: Backend>(pub(crate) G, pub(crate) Universe);

impl<G: Backend> GameContext<G> {
    pub fn run<S: GameState<G>>(graphics_builder: G::Builder) -> GameResult<(), G> {
        let (graphics, events_loop) = graphics_builder
            .build().map_err(GameError::GraphicsError)?;

        let universe = Universe::new();

        G::run::<S>(Self(graphics, universe), events_loop)
    }
}

#[derive(Debug)]
pub enum GameError<G: Backend> {
    GraphicsError(G::Error),
    ResourceFileNotFound(String),
    InvalidTexturesFile,
    TextureNotFound(String),
}

pub type GameResult<T, G> = Result<T, GameError<G>>;

pub trait GameState<G: Backend>: Sized {
    fn init(ctx: &mut GameContext<G>) -> GameResult<Self, G>;
    fn update(&mut self, ctx: &mut GameContext<G>) -> GameResult<(), G>;
    fn draw(&mut self, ctx: &mut GameContext<G>) -> GameResult<(), G>;
}
