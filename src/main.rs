#[macro_use]
extern crate glium;

use crate::graphics::update_and_use_camera;
use crate::{
    game_state::GameState,
    graphics::{
        backend::BackendBuilder,
        clear_color, draw,
        opengl::{backend::OpenGLBackend, camera::Camera as GLCamera, sprite_batch},
        sprite_batch::SpriteBatch,
        DisplayMode, DrawOptions,
    },
};
use crate::graphics::backend::Backend;
use crate::game_state::{GameContext, GameResult};
use crate::world::layers::tile_layer::TileLayer;
use array2d::Array2D;
use crate::world::{Layer, World};

pub mod game_state;
pub mod graphics;
pub mod utils;
pub mod world;

struct Game {
    sb: sprite_batch::SpriteBatch,
    camera: GLCamera,
    world: World<OpenGLBackend>,
}

impl GameState<OpenGLBackend> for Game {
    fn init(ctx: &mut GameContext<OpenGLBackend>) -> GameResult<Game, OpenGLBackend> {
        let tiles = ctx.0.load_texture("tiles")?;
        let sb = sprite_batch::SpriteBatch::new(tiles, ctx);

        let tl = {
            let t = true;
            let f = false;

            TileLayer::autotile(tiles, Array2D::from_rows(&[
                vec![t, t, f, t, t],
                vec![t, t, t, f, t],
                vec![f, t, t, f, t],
                vec![t, f, f, t, t],
                vec![t, t, t, t, t],
            ]), (0,0), 16, 2.0)
        };

        let world = World::new(vec![
            Layer::Tiles(tl)
        ]);

//        println!("{}", serde_yaml::to_string(&tl).unwrap());

        Ok(Game {
            camera: GLCamera::new(),
            sb,
            world
        })
    }

    fn update(&mut self, _ctx: &mut GameContext<OpenGLBackend>) -> GameResult<(), OpenGLBackend> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut GameContext<OpenGLBackend>) -> GameResult<(), OpenGLBackend> {
        update_and_use_camera(ctx, &mut self.camera);
        clear_color(ctx, 5, 5, 10)?;

        self.world.draw(ctx)?;

//        self.sb.reset();
//        self.sb.add([0, 0], [16, 16], DrawOptions::default().position((16.0, 16.0)));
//        draw(ctx, &self.sb, DrawOptions::default())?;

        Ok(())
    }
}

fn main() {
    let graphics_builder = OpenGLBackend::new()
        .title("game")
        .display_mode(DisplayMode::Windowed(800, 450));

    GameContext::run::<Game>(graphics_builder).unwrap();
}
